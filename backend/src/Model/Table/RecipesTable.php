<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;

/**
 * Recipes Model
 *
 * @property \App\Model\Table\CategoriesTable|\Cake\ORM\Association\BelongsTo $Categories
 *
 * @method \App\Model\Entity\Recipe get($primaryKey, $options = [])
 * @method \App\Model\Entity\Recipe newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\Recipe[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Recipe|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Recipe patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Recipe[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\Recipe findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class RecipesTable extends Table
{

    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('recipes');
        $this->setDisplayField('name');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');

        $this->belongsTo('Categories', [
            'foreignKey' => 'category_id',
            'joinType' => 'INNER'
        ]);
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->integer('id')
            ->allowEmpty('id', 'create');
        
        $validator
            ->scalar('category_id')
            ->notEmpty('category_id');
      
        $validator
            ->scalar('name')
            ->notEmpty('name')
            ->add('name', [
                'length' => [
                    'rule' => ['minLength', 10],
                    'message' => 'Recipe names need to be at least 10 characters long',
                ]
            ]);

        $validator
            ->scalar('ingredients')
            ->allowEmpty('ingredients');
      
        $validator
            ->scalar('ingredient_string')
            ->notEmpty('ingredient_string');

        $validator
            ->scalar('steps')
            ->notEmpty('steps')
            ->add('steps', [
                'length' => [
                    'rule' => ['minLength', 50],
                    'message' => 'Recipe description must have a substantial body',
                ]
            ]);

        $validator
            ->scalar('time')
            ->notEmpty('time');

        $validator
            ->boolean('public')
            ->allowEmpty('public');

        return $validator;
    }
  
    public function beforeSave($event, $entity, $options)
    {
        if ($entity->ingredient_string) {
            $entity->ingredients = $this->_buildIngredients($entity->ingredient_string);
        }
    }

    protected function _buildIngredients($ingredientsString)
    {
        // Trim ingredients
        $ingredients = array_map('trim', explode(',', $ingredientsString));
        // Remove all empty ingredients
        $ingredients = array_filter($ingredients);
        // Reduce duplicated ingredients
        $ingredients = array_unique($ingredients);
        // Encode back to JSON
        return json_encode($ingredients);
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->existsIn(['category_id'], 'Categories'));

        return $rules;
    }
}
