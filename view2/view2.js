'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/detail/:recipeId', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$http', '$routeParams',
      function RecipeDetailController($http, $routeParams) {
        var self = this;
        self.tst = $routeParams.recipeId;
        
        $http.get('/backend/api/recipes/detail/' + $routeParams.recipeId).then(function(response) {
        self.detail = response.data.recipe;
        self.ingredients = JSON.parse(response.data.recipe.ingredients);
        });
        
      }]
  
  
);