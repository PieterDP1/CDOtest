## Synopsis

Test recipes app made with CakePHP and AngularJS

## Installation

Add database credentials in backend/config/app.php (template: app_default.php)
Database structure is to be found in backend/config/schema/database.sql
