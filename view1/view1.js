'use strict';

angular.module('myApp.view1', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/list', {
    templateUrl: 'view1/view1.html',
    controller: 'View1Ctrl'
  });
}])

.controller('View1Ctrl', ['$scope', '$http', function ($scope, $http) {
 var self = this;
 $http.get('/backend/api/recipes/').then(function(response) {
        self.recipes = response.data.recipes;
 });
 $http.get('/backend/api/categories/').then(function(response) {
        self.categories = response.data.category;
 });
}]);