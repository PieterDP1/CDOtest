<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Collection\Collection;
/**
 * Recipe Entity
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property string $ingredients
 * @property string $steps
 * @property string $time
 * @property bool $public
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 *
 * @property \App\Model\Entity\Category $category 
 */
class Recipe extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'category_id' => true,
        'name' => true,
        'ingredients' => true,
        'steps' => true,
        'time' => true,
        'public' => true,
        'created' => true,
        'modified' => true,
        'category' => true,
        'ingredient_string' => true
    ];
  
    protected function _getIngredientString()
    {
      if (isset($this->_properties['ingredient_string'])) {
          return $this->_properties['ingredient_string'];
      }
      if (empty($this->ingredients)) {
          return '';
      }
      
      // json_decode ingredients from database
      $ingredients = new Collection(json_decode($this->ingredients,true));
      $str = $ingredients->reduce(function ($string, $ingr) {
          // add commas for human readability 
          return $string . $ingr . ', ';
      }, '');
      // remove tail comma
      return trim($str, ', ');
    }
}
